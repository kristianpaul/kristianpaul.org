---
layout: page
title: Contact
permalink: /contact/
---

If knowledge does not have owners, then intellectual property is a trap set by neo-liberalism.
-- Hugo Chavez, September 28, 2004 

Mobile Phone:

(+57) 300 305-7520

To be used if other methods failed.

GPG Key ID:

7ed1f322
GPG Fingerprint:

14F0 C924 2D6B EED5 8FF6 D6AA 3806 6B2B 7ED1 F322
GPG Public Key:

KEY

Skype

kristian.paul


Or look for me in liure.uk.to IRC
